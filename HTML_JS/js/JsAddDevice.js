/**
 * Created by mrmar on 07.12.2017.
 */

 function goToHome(){
 	window.location.replace("Home.html");
 }

 function goToBarcodes(){
  	window.location.replace("CreateBarcodes.html");
  }

function postNewDevice()
{
    var http = new XMLHttpRequest();
    var url = "http://localhost:8081/ausleihsystem/rs/device";
    var params = JSON.stringify({deviceNr:document.getElementById('deviceNr').value,
        deviceName:document.getElementById('deviceName').value,item:null});
    http.open("POST", url, true);

    //Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/json");

    http.send(params);
}
