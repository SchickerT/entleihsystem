function showInvisDevices(){
  $("#devices").empty();

    $("#devices").append(
        "<table id='devicesTable'>" +
        "<tr>" +
        "<th>Device Name</th>" +
        "<th>Description</th>" +
        "<th>Barcode</th>" +
        "</tr>" +
        "</table>");

    $.ajax({url: "http://localhost:8081/ausleihsystem/rs/device/invisD",
        type: "GET",
        contentType: "application/json-rpc;charset=UTF-8",
        success: function (data) {
            for (var c in data) {
                var device = "<td>" + data[c].deviceName + "</td><td>" + data[c].deviceDescription +
                "</td><td>" + data[c].barcodeNr + "</td><td><button onclick='giveBack(\"" +
                data[c].id + "\")'>Zurückgeben</button></td>";
                document.getElementById("devicesTable").insertRow(-1).innerHTML = device;
            }
        }
      });
}

function giveBack(deviceId){
  localStorage.setItem("giveBackDeviceId", deviceId);
  var device;

  $.ajax({url: "http://localhost:8081/ausleihsystem/rs/device/" + localStorage.getItem("giveBackDeviceId"),
      type: "GET",
      contentType: "application/json-rpc;charset=UTF-8",
      success: function (dataD) {
        device = dataD;
        console.log(device);
        device[0].visible = true;
        console.log(device[0].visible);

        $.ajax({url: "http://localhost:8081/ausleihsystem/rs/device/",
            type: "PUT",
            contentType: "application/json",
            data: JSON.stringify(device[0]),
            success: function (data) {
              console.log(data);
              alert("Erfolgreich zurückgeben!");
              showInvisDevices();
            }
          });
      }
    });
}

function goToHome(){
  window.location.replace("Home.html");
}

function goToLogin(){
  window.location.replace("Login.html");
}
