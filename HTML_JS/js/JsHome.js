function showClasses(){
    $("#buttons").empty();
    $("#devices").empty();
    $("#confirm").empty();

    if(localStorage.getItem("last3Classes") != null){
      $("#buttons").append("<div id=\"buttonsLast3\">Letzten 3 Klassen: </br></div><div id=\"buttonsHifHitm\">Informatik und Medientechnik Klassen: </br></div></br><div id=\"buttonsBgHel\">Medizien und Elekronik Klassen:</br></div>");

      var c = localStorage.getItem("last3Classes");
      var split = c.split(", ");
      for(var v in split){
        var $input = $('<button onclick="showPupils(\'' + split[v]
        + '\')" style="font-size : 20px; width: 150px; height: 100px; margin-bottom: 30px; margin-right: 15px; margin-left: 15px">'
        + split[v] + '</button>');

        $input.appendTo($("#buttonsLast3"));
      }
    }else {
      $("#buttons").append("<div id=\"buttonsHifHitm\">Informatik und Medientechnik Klassen: </br></div></br><div id=\"buttonsBgHel\">Medizien und Elekronik Klassen:</br></div>");

    }

    $.ajax({
        url: "https://mese.webuntis.com/WebUntis/jsonrpc.do?school=htbla linz leonding",
        type: "POST",
        contentType: "application/json-rpc;charset=UTF-8",
        data: JSON.stringify({
            "id": "ID", "method": "authenticate", "params": {
                "user": "in110097", "password": "Nelmecta", "client": "CLIENT"
            },
            "jsonrpc": "2.0"
        }),
        success: function (data) {
            var sId = data['result']['sessionId'];
            $.ajax({
                url: "https://mese.webuntis.com/WebUntis/jsonrpc.do;jsessionid=" + sId,
                type: "POST",
                contentType: "application/json-rpc;charset=UTF-8",
                data: JSON.stringify({"id": "ID", "method": "getKlassen", "jsonrpc": "2.0"}),
                success: function (data) {
                    for (var c in data.result) {
                        if ((data.result[c].name).match("^1")
                            || (data.result[c].name).match("^2")
                            || (data.result[c].name).match("^3")
                            || (data.result[c].name).match("^4")
                            || (data.result[c].name).match("^5")) {

                              if ((data.result[c].name).match("HIF$")) {
                                var name = data.result[c].name;

                                var $input = $('<button onclick="showPupils(\'' + data.result[c].name
                                + '\')" style="font-size : 20px; width: 150px; height: 100px; margin-bottom: 30px; margin-right: 15px; margin-left: 15px">'
                                + data.result[c].name + '</button>');

                                $input.appendTo($("#buttonsHifHitm"));
                              }

                              if ((data.result[c].name).match("HITM$")) {
                                var name = data.result[c].name;

                                var $input = $('<button onclick="showPupils(\'' + data.result[c].name
                                + '\')" style="font-size : 20px; width: 150px; height: 100px; margin-bottom: 30px; margin-right: 15px; margin-left: 15px">'
                                + data.result[c].name + '</button>');

                                $input.appendTo($("#buttonsHifHitm"));
                              }

                              if ((data.result[c].name).match("BG$")) {
                                var name = data.result[c].name;

                                var $input = $('<button onclick="showPupils(\'' + data.result[c].name
                                + '\')" style="font-size : 20px; width: 150px; height: 100px; margin-bottom: 30px; margin-right: 15px; margin-left: 15px">'
                                + data.result[c].name + '</button>');

                                $input.appendTo($("#buttonsBgHel"));
                              }

                              if ((data.result[c].name).match("HEL$")) {
                                var name = data.result[c].name;

                                var $input = $('<button onclick="showPupils(\'' + data.result[c].name
                                + '\')" style="font-size : 20px; width: 150px; height: 100px; margin-bottom: 30px; margin-right: 15px; margin-left: 15px">'
                                + data.result[c].name + '</button>');

                                $input.appendTo($("#buttonsBgHel"));
                              }
                        }
                    }
                }
            })
        }
    });
}

function showPupils(className) {
    localStorage.setItem("schoolClass", className);

    if (localStorage.getItem("last3Classes") == null) {
      localStorage.setItem("last3Classes", className);
    }else {
      var c = localStorage.getItem("last3Classes");
      var split = c.split(",");

      if(split.length >= 3){
        split.splice(0, 1)
        console.log(split);
        localStorage.setItem("last3Classes", split + ", " + className);
      }else {
        localStorage.setItem("last3Classes", localStorage.getItem("last3Classes") + ", " + className);
      }
    }

    $("#buttons").empty();
    $("#devices").empty();
    $("#confirm").empty();

    $.ajax({url: "http://localhost:8081/ausleihsystem/rs/pupil",
        type: "GET",
        contentType: "application/json-rpc;charset=UTF-8",
        success: function (data) {

            for (var c in data) {
                if ((data[c].schoolClass).match(localStorage.getItem("schoolClass"))) {
                  var $input = $('<button onclick="showDevices(\'' + data[c].id +
                   '\')" style="font-size : 20px; width: 160px; height: 100px; margin-bottom:' +
                   '30px; margin-right: 15px; margin-left: 15px">' + data[c].firstName + '<br> ' +
                   data[c].lastName + '</button>');
                  $input.appendTo($("#buttons"));
                }
            }
        }
    });
}

function showDevices(pupilId){
    localStorage.setItem("pupilId", pupilId);

    $("#buttons").empty();
    $("#confirm").empty();
    $("#devices").append(
        "<table id='devicesTable'>" +
        "<tr>" +
        "<th>Device Name</th>" +
        "<th>Description</th>" +
        "<th>Barcode</th>" +
        "</tr>" +
        "</table>");

    $.ajax({url: "http://localhost:8081/ausleihsystem/rs/device/visD",
        type: "GET",
        contentType: "application/json-rpc;charset=UTF-8",
        success: function (data) {
            for (var c in data) {
                var device = "<td>" + data[c].deviceName + "</td><td>" + data[c].deviceDescription +
                "</td><td>" + data[c].barcodeNr + "</td><td><button onclick='confirmAll(\"" +
                data[c].id + "\")'>Add</button></td>";
                document.getElementById("devicesTable").insertRow(-1).innerHTML = device;
            }
        }
      });
}

function confirmAll(deviceId){
  localStorage.setItem("deviceId", deviceId);

  $("#buttons").empty();
  $("#devices").empty();

  $.ajax({url: "http://localhost:8081/ausleihsystem/rs/pupil/" + localStorage.getItem("pupilId"),
      type: "GET",
      contentType: "application/json-rpc;charset=UTF-8",
      success: function (dataP) {
        console.log(dataP);
        $("#confirm").append(
          "<div id=\"confirmText\">"+
          "<label>Klasse</label><br>"+
          "<label>Schüler</label><br>"+
           "<label>Item</label><br>"+
           "<label>Von</label><br>"+
           "<label>Bis</label></div>");
        $("#confirm").append(
          "<div id=\"confirmFields\">"+
          "<input type=\"text\" value=\"" + localStorage.getItem('schoolClass') + "\" disabled=\"true\"><br>" +
          "<input type=\"text\" value=\"" + dataP.firstName + " " + dataP.lastName + "\" disabled=\"true\"><br>" +
          "<input type=\"text\" value=\"" + deviceId + "\" disabled=\"true\"><br>" +
          "<input id=\"startDate\" type=\"date\"><br>" +
          "<input id=\"endDate\" type=\"date\"><br><br>" +
          "<button onclick=\"pushRent()\">Bestätigen</button></div>"
        );
      }
    });
}

function pushRent(){

  alert("Erfolgreich ausgeliehen!");

  localStorage.setItem("startDate", document.getElementById("startDate").value);
  localStorage.setItem("endDate", document.getElementById("endDate").value);

  var device;
  var pupil;
  var startDate = localStorage.getItem("startDate");
  var endDate = localStorage.getItem("endDate");

  $.ajax({url: "http://localhost:8081/ausleihsystem/rs/device/" + localStorage.getItem("deviceId"),
      type: "GET",
      contentType: "application/json-rpc;charset=UTF-8",
      success: function (dataD) {
        device = dataD;
        console.log(device);

        $.ajax({url: "http://localhost:8081/ausleihsystem/rs/pupil/" + localStorage.getItem("pupilId"),
            type: "GET",
            contentType: "application/json-rpc;charset=UTF-8",
            success: function (dataP) {
              pupil = dataP;
              console.log(pupil);
              var jsonString = JSON.stringify({"borrowDate": startDate, "returnDate": endDate, "pupil": pupil, "devices": device});
              var json = JSON.parse(jsonString);
              console.log(json.devices[0].visible);
              console.log(json);
              $.ajax({url: "http://localhost:8081/ausleihsystem/rs/rent/rentDevice",
                  type: "POST",
                  contentType: "application/json",
                  data: jsonString,
                  success: function (data) {
                    console.log("Funzt");
                  }
                });
            }
          });
      }
    });

    showClasses();
}

function goToLogin(){
  window.location.replace("Login.html");
}

function goToGiveBack(){
  window.location.replace("GiveBack.html");
}
