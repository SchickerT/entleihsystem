package at.htl.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.org.apache.regexp.internal.RE;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mrmar on 26.01.2018.
 */
@Table(name ="E_Pupil")
@Entity
@XmlRootElement
public class Pupil{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String schoolClass;
    private String email;

    @JsonBackReference
    @OneToMany(mappedBy = "pupil",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<Rent> rentList = new LinkedList<Rent>();



    public Pupil( String schoolClass, String lastName,String firstName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.schoolClass = schoolClass;
    }

    public Pupil() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(String schoolClass) {
        this.schoolClass = schoolClass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Rent> getRentList() {
        return rentList;
    }

    public void setRentList(List<Rent> rentList) {
        this.rentList = rentList;
    }
}
