package at.htl.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.org.apache.regexp.internal.RE;

import javax.persistence.*;
import javax.ws.rs.DELETE;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;
@XmlRootElement
@Table(name ="E_Device")
@Entity
public class Device{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int barcodeNr;
    private String deviceName;
    private String deviceDescription;
    private boolean visible;

    @ManyToOne(cascade = CascadeType.ALL )
    private Device device;

    @JsonBackReference
    @OneToMany(mappedBy="device",cascade = CascadeType.ALL)
    private List<Device> devices;



    @ManyToOne
    @JoinColumn(name = "Rent_ID")
    private Rent rent;

    public Device(int barcodeNr, String deviceName, String deviceDescription, boolean visible) {
        this.barcodeNr = barcodeNr;
        this.deviceName = deviceName;
        this.deviceDescription = deviceDescription;
        this.visible = visible;
    }

    public Device() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getBarcodeNr() {
        return barcodeNr;
    }

    public void setBarcodeNr(int barcodeNr) {
        this.barcodeNr = barcodeNr;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceDescription() {
        return deviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        this.deviceDescription = deviceDescription;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public Rent getRent() {
        return rent;
    }

    public void setRent(Rent rent) {
        this.rent = rent;
    }
}
