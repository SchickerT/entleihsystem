package at.htl.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.sql.Date;
import java.util.LinkedList;
import java.util.List;
@Table(name ="E_Rent")
@Entity
@XmlRootElement
public class Rent implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date borrowDate;
    private Date returnDate;


    @OneToMany(mappedBy = "rent",cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
    @JsonBackReference
    private List<Device> devices = new LinkedList<Device>();

    @ManyToOne
    @JoinColumn(name = "personrent")
    private Pupil pupil;



    public Rent(Date borrowDate, Date returnDate, List<Device> devices, Pupil pupil) {
        this.borrowDate = borrowDate;
        this.returnDate = returnDate;
        this.devices = devices;
        this.pupil = pupil;
    }

    public Rent() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public Pupil getPupil() {
        return pupil;
    }

    public void setPupil(Pupil pupil) {
        this.pupil = pupil;
    }

    @Override
    public String toString() {
        return "Rent{" +
                "id=" + id +
                ", borrowDate=" + borrowDate +
                ", returnDate=" + returnDate +
                ", devices=" + devices +
                ", pupil=" + pupil +
                '}';
    }
}
