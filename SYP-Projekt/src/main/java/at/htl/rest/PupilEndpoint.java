package at.htl.rest;

import at.htl.business.PupilFacade;
import at.htl.model.Pupil;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mrmar on 26.01.2018.
 */
@Path("/pupil")
public class PupilEndpoint {

    @Inject
    PupilFacade pupilFacade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllPupils(){
        List<Pupil> pupilList = new LinkedList<Pupil>();
        pupilList = pupilFacade.getSchueler();
        return  Response.ok(pupilList).header("Access-Control-Allow-Origin", "*").build();
    }
}
