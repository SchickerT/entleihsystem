package at.htl.rest;

import at.htl.business.UserFacade;
import at.htl.model.User;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("login")
public class LoginEndpoint {

    @Inject
    UserFacade userFacade;


    @POST
    @Path("/checkuser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response queryByIdentification(User user, @Context UriInfo uriInfo){
        //User user = userFacade.getUser(userName,password);
        List<User> users = userFacade.getUsers();
        for (int i = 0; i <users.size(); i++) {
            if(users.get(i).getUserName().equals(user.getUserName()) && users.get(i).getUserPassword().equals(user.getUserPassword())){
                return Response.status(200).entity(user).build();
            }
        }
        System.out.println("++++++++++++++++++++++++++++++++++++++++");
        return Response.status(400).build();
    }
}
