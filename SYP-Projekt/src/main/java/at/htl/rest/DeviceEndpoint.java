package at.htl.rest;


import at.htl.business.Barcodes;
import at.htl.business.DeviceFacade;
import at.htl.model.Device;
import com.lowagie.text.DocumentException;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static at.htl.business.Barcodes.DEST;

/**
 * Created by cristian on 16.11.2017.
 */

@Path("/device")
public class DeviceEndpoint {

    @Inject
    DeviceFacade deviceFacade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllVisableDevices(){
        List<Device> deviceList = new LinkedList<Device>();
        List<Device> visableDeviceList = new LinkedList<Device>();
        deviceList = deviceFacade.getDevices();
        for (Device device: deviceList) {
            if(device.isVisible()){
                visableDeviceList.add(device);
            }
        }
        return  Response.ok(visableDeviceList).header("Access-Control-Allow-Origin", "*").build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createDevice(Device device,@Context UriInfo uriInfo)
    {
        if(device == null){
            return Response.status(400).header("reason","Device darf nicht NULL sein").build();
        }
        deviceFacade.create(device);
        return Response.ok().header("Access-Control-Allow-Origin", "*").entity(device).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateDevice(Device device,@Context UriInfo uriInfo)
    {
        if(device == null){
            return Response.status(400).header("reason","Device darf nicht NULL sein").build();
        }
        deviceFacade.create(device);
        return Response.ok().header("Access-Control-Allow-Origin", "*").entity(device).build();
    }

    @GET
    @Path("/barcodepdf")
    @Produces("application/pdf")
    public Response getFile() throws IOException, DocumentException {

        Barcodes.createPdf(DEST,Integer.valueOf(deviceFacade.getLastItemNumber())+1);
        File currDir = new File(".");
        String path = currDir.getAbsolutePath();
        System.out.println("----------------------------"+path);
        return Response.ok(new File(DEST)).build();
    }

    @GET
    @Path("/getlastdevicenr")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLastItemNumber(){
        String item;
        item = deviceFacade.getLastItemNumber();

        return Response.ok().header("Access-Control-Allow-Origin", "*").entity(item).build();
    }
}
