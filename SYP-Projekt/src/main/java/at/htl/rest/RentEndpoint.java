package at.htl.rest;


import at.htl.business.DeviceFacade;
import at.htl.business.RentFacade;
import at.htl.model.Device;
import at.htl.model.Pupil;
import at.htl.model.Rent;
import com.sun.org.apache.regexp.internal.RE;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;

@Path("rent")
public class RentEndpoint
{
    @Inject
    RentFacade rentFacade;

    @Inject
    DeviceFacade deviceFacade;
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllRents() {
        List<Rent> rentList;
        rentList = rentFacade.getRents();
        return Response.ok(rentList).header("Access-Control-Allow-Origin", "*").build();
    }

    @POST //TODO: zu GET machen
    @Path("/rentbypupil")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getRentByPupilId(Pupil pupil){
        List<Rent> rentList;
        rentList = rentFacade.getPupilRents(pupil.getId());
        return  Response.ok(rentList).header("Access-Control-Allow-Origin", "*").build();
    }

    @POST
    @Path("/rentDevice")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response rentDevice(Rent rent){
        if(rent == null){
            return Response.status(400).header("reason","Rent darf nicht NULL sein").build();
        }
        for (Device device: rent.getDevices()) {
                device.setVisible(false);
                deviceFacade.create(device);
        }
        rentFacade.create(rent);
        return Response.ok().header("Access-Control-Allow-Origin", "*").entity(rent).build();
    }
}
