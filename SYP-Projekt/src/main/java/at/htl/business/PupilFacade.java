package at.htl.business;

import at.htl.model.Pupil;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by mrmar on 26.01.2018.
 */
@Stateless
public class PupilFacade {

    @PersistenceContext
    EntityManager em;

    public void create(Pupil pupil){
        if(pupil.getId() != null && em.find(Pupil.class, pupil.getId())!= null){
            em.merge(pupil);
        }
        else {
            em.persist(pupil);
        }
    }

    public List<Pupil> getSchueler(){
        TypedQuery<Pupil> pupilTypedQuery= em.createQuery("select s from Pupil s", Pupil.class);
        return  pupilTypedQuery.getResultList();
    }

    public Pupil getPupil(Long id){
        TypedQuery<Pupil> schuelerTypedQuery= em.createQuery("select s from Pupil s where s.id = :id", Pupil.class);
        schuelerTypedQuery.setParameter("id",id);
        Pupil pupil = schuelerTypedQuery.getSingleResult();
        return  schuelerTypedQuery.getSingleResult();
    }
}
