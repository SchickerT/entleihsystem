package at.htl.business;

import at.htl.model.Device;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;

@Stateless
public class DeviceFacade implements Serializable{
    @PersistenceContext
    EntityManager em;

    public void create(Device device){
        if(device.getId() != null && em.find(Device.class, device.getId())!= null){
            em.merge(device);
        }
        else {
            em.persist(device);
        }
    }
    public List<Device> getDevices(){
        TypedQuery<Device> deviceTypedQuery= em.createQuery("select d from Device d", Device.class);
        return  deviceTypedQuery.getResultList();
    }

    public String getLastItemNumber()
    {
        int highestNr=1;
        List<Device> deviceList = getDevices();
        for (Device device:deviceList) {
            if(device.getBarcodeNr() > highestNr){
                highestNr=device.getBarcodeNr();
            }
        }
        return String.valueOf(highestNr);
    }

}
