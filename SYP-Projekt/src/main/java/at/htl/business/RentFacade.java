package at.htl.business;

import at.htl.model.Rent;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class RentFacade {
    @PersistenceContext
    EntityManager em;

    public void create(Rent rent){
        if(rent.getId() != null && em.find(Rent.class, rent.getId())!= null){
            em.merge(rent);
        }
        else {
            em.persist(rent);
        }
    }
    public List<Rent> getRents(){
        TypedQuery<Rent> rentTypedQuery = em.createQuery("select d from Rent d", Rent.class);
        return  rentTypedQuery.getResultList();
    }

    public List<Rent> getPupilRents(Long pupilId){
        TypedQuery<Rent> rentTypedQuery = em.createQuery("select d from Rent d where d.pupil.id = :pupilId", Rent.class);
        rentTypedQuery.setParameter("pupilId",pupilId);
        return  rentTypedQuery.getResultList();
    }
}
