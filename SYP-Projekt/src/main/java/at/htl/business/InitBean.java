package at.htl.business;

import at.htl.model.*;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.io.*;
import java.sql.Date;
import java.text.ParseException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

@Singleton
@Startup
public class InitBean {

    @Inject
    DeviceFacade deviceFacade;

    @Inject
    UserFacade userFacade;

    @Inject
    RentFacade rentFacade;

    @Inject
    PupilFacade pupilFacade;

    public InitBean() {
    }

    @PostConstruct
    private void init() throws ParseException {

        String csvFile = "D:\\School\\5CHIF\\SYP\\Projekt\\entleihsystem\\SYP-Projekt\\schueler.txt";
        String line = "";
        String cvsSplitBy = ";";

        try (BufferedReader br = new BufferedReader(new InputStreamReader
                (new FileInputStream(csvFile), "UTF-8"))){

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] col = line.split(cvsSplitBy);
                Pupil s = new Pupil(col[0],col[1],col[2]);
                pupilFacade.create(s);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        userFacade.create(new User("Cristian3412","1234"));

        Device component1 = new Device(2,"bestandTeilRoboter1","1112",true);
        Device component2 = new Device(3,"bestandTeilRoboter2","1113",true);
        List<Device> componentList = new LinkedList<>();
        componentList.add(component1);
        componentList.add(component2);
        Device robot1 = new Device(1,"Roboter","1111",true);
        Device robot4 = new Device(4,"Roboter4","1114",true);

        component1.setDevice(robot1);
        component2.setDevice(robot1);

        deviceFacade.create(component1);
        deviceFacade.create(component2);
        deviceFacade.create(robot1);

        deviceFacade.create(robot4);

        List<Device> deviceList1 = new LinkedList<Device>();
        deviceList1.add(robot1);
        deviceList1.add(component1);
        deviceList1.add(component2);
        deviceList1.add(robot4);

        List<Device> deviceList2 = new LinkedList<Device>();
        deviceList1.add(robot1);
        deviceList1.add(component1);

        Calendar currentTime = Calendar.getInstance();
        Date sqlDate = new Date((currentTime.getTime().getTime()));

        Rent rent1 = new Rent(sqlDate,new java.sql.Date(2017, 10, 23),deviceList1, pupilFacade.getPupil(Long.valueOf(1)));
        Rent rent2 = new Rent(sqlDate,new java.sql.Date(2017, 10, 23),deviceList2, pupilFacade.getPupil(Long.valueOf(3)));
        Rent rent3 = new Rent(sqlDate,new java.sql.Date(2017, 10, 23),deviceList2, pupilFacade.getPupil(Long.valueOf(7)));
        deviceFacade.create(robot1);


        rentFacade.create(rent1);
        rentFacade.create(rent2);

        List<Rent> rentList = new LinkedList<Rent>();
        rentList.add(rent1);
        rentList.add(rent2);
        rentList.add(rent3);
    }
}
