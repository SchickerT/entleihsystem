package at.htl.business;


import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.*;

public class Barcodes {

    public static final String DEST = "C:/Temp/barcode_table.pdf";

    File file = new File(DEST);

    //file.getParentFile().mkdirs();



    public static void createPdf(String dest,int input) throws IOException, DocumentException
    {
        File temp = new File("C:/Temp");
        temp.mkdir();

        Document document = new Document();

        PdfWriter writer = PdfWriter
                .getInstance(document, new FileOutputStream(DEST));

        document.open();


        //column_size
        PdfPTable table = new PdfPTable(4);

        //Barcode_width
        table.setWidthPercentage(100);

        for (int i = 0; i <= 36; i++)
        {
            table.addCell(createBarcode(writer, String.format("%08d", input+i)));
        }
//        ColumnText columnText = new ColumnText(writer.getDirectContent());
//        columnText.addElement(table);
        document.add(table);
        document.add(new Chunk(""));
        document.close();

    }

    public static PdfPCell createBarcode(PdfWriter writer, String code) throws DocumentException, IOException {
        Barcode128 barcode = new Barcode128();
        barcode.setCodeType(Barcode.CODE128);
        barcode.setCode(code);
        PdfPCell cell = new PdfPCell(barcode.createImageWithBarcode(writer.getDirectContent(), Color.BLACK, Color.GRAY), true);
        cell.setPadding(5);
        return cell;
    }
}
