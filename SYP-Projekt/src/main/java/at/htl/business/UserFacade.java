package at.htl.business;

import at.htl.model.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Stateless
public class UserFacade {
    @PersistenceContext
    EntityManager em;

    public String hashPassword(String passwordToHash){
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
                    md.update(passwordToHash.getBytes());
            byte byteData[] = md.digest();

            //convert the byte to hex format method 1
            StringBuffer hashCodeBuffer = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                hashCodeBuffer.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return hashCodeBuffer.toString();
        }
        catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        return generatedPassword;
    }

    public void create(User user){
        if(user.getId()!= null && em.find(User.class, user.getId())!= null){
            em.merge(user);
        }
        else {
            user.setUserPassword(hashPassword(user.getUserPassword()));
            em.persist(user);
        }
    }

    public List<User> getUsers(){
        TypedQuery<User> userTypedQuery = em.createQuery("select u from User u",User.class);
        return  userTypedQuery.getResultList();
    }

    public User getUser(String userName, String password){
        TypedQuery<User> userTypedQuery = em.createQuery("select u from User u where u.userName = :userName", User.class);
        userTypedQuery.setParameter("userName",userName );
        return userTypedQuery.getSingleResult();
    }
}
